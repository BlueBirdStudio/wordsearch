package com.xyrality.wordsearch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xyrality.wordsearch.R;
import com.xyrality.wordsearch.model.WordsData;

import java.util.List;

public class WordsAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    List<WordsData> allAvailableWorlds;

    public WordsAdapter(Context context, List<WordsData> allAvailableWorlds) {
        ctx = context;
        this.allAvailableWorlds = allAvailableWorlds;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // count of elements
    @Override
    public int getCount() {
        return allAvailableWorlds.size();
    }

    @Override
    public Object getItem(int position) {
        return allAvailableWorlds.get(position);
    }

    // id position
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_item, parent, false);
        }

        String name = allAvailableWorlds.get(position).getName();

        ((TextView) view.findViewById(R.id.tvItem)).setText(name);
        return view;
    }
}
