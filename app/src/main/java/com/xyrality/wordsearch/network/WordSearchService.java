package com.xyrality.wordsearch.network;

import com.xyrality.wordsearch.model.FindWordsResponse;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.QueryMap;

public interface WordSearchService {

    @POST("/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds")
    void findWords(@QueryMap Map<String, String> params, @Header("Authorization") String auth, Callback<FindWordsResponse> response);
}
