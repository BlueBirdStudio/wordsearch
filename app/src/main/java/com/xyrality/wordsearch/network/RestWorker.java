package com.xyrality.wordsearch.network;

import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.xyrality.wordsearch.model.FindWordsResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;

public class RestWorker {

    public static RestWorker restWorker;

    private static volatile WordSearchService wordRestClient;

    public final String LOG_TAG = RestWorker.class.getName();

    public final String URL = "http://backend1.lordsandknights.com";

    Gson gson;

    public static RestWorker getInstance() {
        if (restWorker == null) {
            synchronized (RestWorker.class) {
                if (restWorker == null) {
                    restWorker = new RestWorker();
                }
            }
        }
        return restWorker;
    }

    private RestWorker() {
        createRestWorker();
    }

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", "application/json");
            request.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        }
    };

    private void createRestWorker() {
        gson = new Gson();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()))
        .setLog(new AndroidLog("WORDS"))
                .setEndpoint(URL)
                .setRequestInterceptor(requestInterceptor)
                .build();

        wordRestClient = restAdapter.create(WordSearchService.class);
    }

    public void findWords(String login, String password, String deviceType, String deviceId, Callback<FindWordsResponse> cb) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("login", login);
        params.put("password", password);
        params.put("deviceType", deviceType);
        params.put("deviceId", deviceId);

        String credentials = login + ":" + password;
        String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        wordRestClient.findWords(params, "Basic " + base64EncodedCredentials, cb);
    }
}
