package com.xyrality.wordsearch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xyrality.wordsearch.model.FindWordsResponse;
import com.xyrality.wordsearch.network.RestWorker;

import java.io.Serializable;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        final EditText etEmail = (EditText) findViewById(R.id.etEmail);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);

        Button btnSearch = (Button) findViewById(R.id.btnSearch);

        final String deviceType = String.format("%s %s", android.os.Build.MODEL, android.os.Build.VERSION.RELEASE);

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        final String macAddress = wInfo.getMacAddress();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestWorker.getInstance().findWords(etEmail.getText().toString(), etPassword.getText().toString(), deviceType, macAddress, new Callback<FindWordsResponse>() {
                    @Override
                    public void success(FindWordsResponse findWordsResponse, Response response) {
                        startActivity(new Intent(SearchActivity.this, WordsActivity.class).putExtra("WORDS",(Serializable)findWordsResponse));
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(SearchActivity.this, "Error", Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}
