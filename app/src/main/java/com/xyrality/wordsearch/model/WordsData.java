package com.xyrality.wordsearch.model;

import java.io.Serializable;

public class WordsData implements Serializable {

    String country;
    long id;
    String language;
    String mapURL;
    String name;
    String url;

    WordsStatusData worldStatus;

    public String getCountry() {
        return country;
    }

    public long getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }

    public String getMapURL() {
        return mapURL;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public WordsStatusData getWorldStatus() {
        return worldStatus;
    }
}
