package com.xyrality.wordsearch.model;

import java.io.Serializable;
import java.util.List;

public class FindWordsResponse implements Serializable {

    String serverVersion;

    List<WordsData> allAvailableWorlds;

    public String getServerVersion() {
        return serverVersion;
    }

    public List<WordsData> getAllAvailableWorlds() {
        return allAvailableWorlds;
    }
}
