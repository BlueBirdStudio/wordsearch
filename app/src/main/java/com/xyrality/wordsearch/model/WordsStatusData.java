package com.xyrality.wordsearch.model;

import java.io.Serializable;

public class WordsStatusData implements Serializable {

    long id;
    String description;

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
