package com.xyrality.wordsearch;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.xyrality.wordsearch.adapter.WordsAdapter;
import com.xyrality.wordsearch.model.FindWordsResponse;
import com.xyrality.wordsearch.model.WordsData;

import java.util.List;

public class WordsActivity extends Activity {

    private List<WordsData> allAvailableWorlds;
    private FindWordsResponse findWordsResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView wordsListView = (ListView) findViewById(R.id.wordsListView);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                findWordsResponse = null;
            } else {
                findWordsResponse= (FindWordsResponse)extras.getSerializable("WORDS");
            }
        } else {
            findWordsResponse = (FindWordsResponse)savedInstanceState.getSerializable("WORDS");
        }

        if (findWordsResponse != null)
            allAvailableWorlds = findWordsResponse.getAllAvailableWorlds();

        WordsAdapter wordsAdapter = new WordsAdapter(this, allAvailableWorlds);
        wordsListView.setAdapter(wordsAdapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("WORDS", findWordsResponse);
        super.onSaveInstanceState(outState);
    }
}
